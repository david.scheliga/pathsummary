***************************
API reference
***************************

.. autosummary::
   :toctree:

   pathsummary
   pathsummary.summarize_folder_files
   pathsummary.count_type_occurrences


.. toctree::

   PathSummary/index
