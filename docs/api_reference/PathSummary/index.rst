***************************
pathsummary.PathSummary
***************************

.. autosummary::
   :toctree:

   pathsummary.PathSummary.empty
   pathsummary.PathSummary.summarize_path
   pathsummary.PathSummary.from_file_paths
   pathsummary.PathSummary.iter_by_filetype
   pathsummary.PathSummary.count_type_occurrences
   pathsummary.PathSummary.table
