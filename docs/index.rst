.. isisysvic3daccess documentation master file, created by
   sphinx-quickstart on Fri Sep 25 10:54:55 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=============================================================================
Welcome to 'pathsummary' documentation!
=============================================================================

Path summary is a helper module to summarize the content of a folder based
on the mimetypes (file types and sub types).

.. image:: ../pathsummary-icon.svg
   :height: 196px
   :width: 196px
   :alt: A trash panda.
   :align: center

Installation
============

Install the latest release from pip.

.. code-block:: shell

   $ pip install pathsummary

.. toctree::
   :maxdepth: 3

   api_reference/index

Indices and tables
==================

* :ref:`genindex`
